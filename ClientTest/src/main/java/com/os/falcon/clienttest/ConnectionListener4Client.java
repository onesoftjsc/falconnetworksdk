/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.clienttest;

import com.os.falcon.net.sdk.service.ConnectionListenerForClient;
import com.os.falcon.net.sdk.service.FalconNetworkSDK;
import com.os.falcon.net.sdk.util.LogUtil;
import io.socket.client.Socket;

/**
 *
 * @author quanph
 */
public class ConnectionListener4Client extends ConnectionListenerForClient{

    @Override
    public void onConnected(Socket socket) {
        FalconNetworkSDK.sendToServer(socket, new CSPing("Hello From Client!"));
    }

    @Override
    public void onError(Socket socket) {
        LogUtil.info("Error: " + socket);
    }
    
}
