/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.clienttest;

import com.os.falcon.net.sdk.service.FalconNetworkSDK;
import com.os.falcon.net.sdk.threadmanagement.FThreadManager;
import io.socket.client.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 * Lưu ý: Không bao giờ được đặt tên của event trùng với các tên có sẵn như
 * (connect,ping,disconnect,...). Đặt trùng là không có chạy đc, tìm lỗi bở hơi.
 *
 * @author quanph <quanph@onesoft.com.vn>
 */
public class Main {

    public static void main(String[] args) throws Exception {
//        Socket socket1 = FalconNetworkSDK.connectTo("http://localhost:10443/test");

        Socket socket1 = FalconNetworkSDK.connectTo("http://dwh.data4game.com:10443/test");
        Socket socket2 = FalconNetworkSDK.connectTo("http://datanode1:10443/test");
//        Socket socket1 = FalconNetworkSDK.connectTo("http://192.168.1.83:10443/test");
//        int count = 0;
//        while (true) {
//            count++;
//            FalconNetworkSDK.sendToServer(socket1, new CSPing("" + count));
//            Thread.sleep(1000);
//        }

//        Socket socket2 = FalconNetworkSDK.connectTo("http://datanode1:10443/test");
        FThreadManager.getInstance().start();
        for (int i = 0; i < 10; i++) {

            new Thread(() -> {
                while (true) {
                    if (!socket1.connected()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                        }
                    } else {
                        synchronized (MonitorThread.class) {
                            MonitorThread.count++;
                            if (MonitorThread.count >= 5000000) {
                                break;
                            }
//                            FalconNetworkSDK.sendToServer(socket1, new CSPing("Hello!!!"));
                                socket1.emit("test", new JSONObject(new CSPing("Hello!!!")));
                        }

                    }
                }
            }).start();
        }
//
        while (MonitorThread.count < 5000000) {
            Thread.sleep(100);
        }

        new Thread(() -> {
            while (true) {
                if (!socket2.connected()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                    }
                } else {
                    synchronized (MonitorThread.class) {
                        MonitorThread.count++;
                        if (MonitorThread.count >= 5100000) {
                            break;
                        }
//                        FalconNetworkSDK.sendToServer(socket2, new CSPing("Hello!!!"));
                          socket2.emit("test", new JSONObject(new CSPing("Hello!!!")));
                    }

                }
            }
        }).start();

    }
    
}
