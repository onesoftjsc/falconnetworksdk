/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.clienttest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.os.falcon.net.sdk.message.Message;

/**
 *
 * @author quanph
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SCPing extends Message {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SCPing(String text) {
        this.text = text;
    }

    public SCPing() {
    }

    @Override
    public String getEvent() {
        return "test";
    }

}
