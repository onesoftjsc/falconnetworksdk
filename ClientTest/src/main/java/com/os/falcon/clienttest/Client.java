/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.clienttest;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 *
 * @author quanph
 */
public class Client {

    ZContext context = new ZContext(1);
    ZMQ.Socket socket = context.createSocket(ZMQ.PAIR);

    public void sendMessage() {
        socket.connect("tcp://dwh.data4game.com:10101");
//        byte[] recv = socket.recv();
//        if (recv != null) {
//            System.out.println("received: " + new String(recv));
//        }
        String str = "Hello";
        for (int i = 0; i < 200; i++)
            str += "Hello";
        for (int i = 0; i < 5000000; i++) {
            boolean b = socket.send(str);
            if (i % 1000 == 0)
                try {
                    Thread.sleep(1);
            } catch (InterruptedException ex) {
            }
        }
    }

    public static void main(String args[]) {
        Client client = new Client();
        client.sendMessage();
    }

}
