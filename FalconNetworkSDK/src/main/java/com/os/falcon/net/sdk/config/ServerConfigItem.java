/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.config;

/**
 *
 * @author quanph
 */
public class ServerConfigItem {
     String serverIp;
     int serverPort;
     String serverContextPath;
     boolean sslOn;
     String sslKeystore;
     String sslPassword;

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }
    
    public static ServerConfigItem createItem(String serverIp, int serverPort, String serverContextPath, boolean sslOn, String sslKeystore, String sslPassword){
        ServerConfigItem item = new ServerConfigItem();
        item.serverIp = serverIp;
        item.serverPort = serverPort;
        item.serverContextPath = serverContextPath;
        item.sslOn = sslOn;
        item.sslKeystore = sslKeystore;
        item.sslPassword = sslPassword;
        return item;
    }


    public String getServerContextPath() {
        return serverContextPath;
    }

    public boolean isSslOn() {
        return sslOn;
    }

    public String getSslKeystore() {
        return sslKeystore;
    }


    public String getSslPassword() {
        return sslPassword;
    }

}
