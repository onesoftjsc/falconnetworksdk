/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.threadmanagement;

import com.os.falcon.net.sdk.annotation.FADailySchedule;
import com.os.falcon.net.sdk.annotation.FAFixedRateSchedule;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.os.falcon.net.sdk.annotation.FAThread;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public abstract class FThread implements IFThread {

    public static final int STATE_NOT_STARTED = 0;
    public static final int STATE_RUNNING = 1;
    public static final int STATE_IN_SCHEDULE = 2;
    public static final int STATE_STOPPED = 3;

    private int state = STATE_NOT_STARTED;
    private int numberStarted = 0;
    private String nextDateToRun = "";

    private Set<String> alreadyRuns = new HashSet<String>();

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
    Thread thread;

    public void start() {
        alreadyRuns.add(nextDateToRun);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FThread.this.onStart();
                FThread.this.process();
                FThread.this.onStop();
            }
        });

        numberStarted++;
        thread.start();
    }

    @Override
    public void onStop() {
        FThreadManager.getInstance().onThreadStop(this);
    }

    /**
     *
     * @return = -1 Nếu ko còn schedule nào thỏa mãn = thời gian theo giây đến
     * lần chạy gần nhất tiếp theo
     */
    public int getNextRuntime() {
        FADailySchedule[] dailySchedules = this.getClass().getAnnotationsByType(FADailySchedule.class);
        if (dailySchedules == null || dailySchedules.length == 0) {
            if (numberStarted == 0) {
                FAThread faT = this.getClass().getAnnotation(FAThread.class);
                return faT.delay();
            } else {
                boolean isFixedRate = this.getClass().isAnnotationPresent(FAFixedRateSchedule.class);
                if (isFixedRate) {
                    FAFixedRateSchedule fixedRate = this.getClass().getAnnotation(FAFixedRateSchedule.class);
                    return (int) fixedRate.timeUnit().toSeconds(fixedRate.period());
                }
            }
        } else {
            long minNextTime = Long.MAX_VALUE;
            for (int i = 0; i < dailySchedules.length; i++) {
                try {
                    String time = dailySchedules[i].time();
                    String dayOfWeek = dailySchedules[i].dayOfWeek();
                    String startDateStr = dailySchedules[i].startDate();
                    String endDateStr = dailySchedules[i].endDate();
                    String todayDateStr = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                    Date startDate = sdf.parse(startDateStr + " " + time);
                    Date endDate = sdf.parse(endDateStr + " " + time);
                    Date todayDate = sdf.parse(todayDateStr + " " + time);

                    Date nowDate = new Date();

                    if (todayDate.before(nowDate)) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(todayDate);
                        c.add(Calendar.DATE, 1);
                        todayDate = c.getTime();
                    }

                    if (todayDate.after(startDate)) {
                        startDate = todayDate;
                    }
                    if (startDate.after(endDate)) {
                        continue;
                    }
                    Date nextDate = null;
                    boolean hasNextDay = false;
                    for (int j = 0; j < 7; j++) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(startDate);
                        c.add(Calendar.DATE, j);
                        nextDate = c.getTime();
                        if (nextDate.after(endDate)) {
                            break;
                        }
                        String nextDateTempStr = sdf.format(nextDate);
                        if (alreadyRuns.contains(nextDateTempStr)) {
                            continue;
                        }
                        int dow = c.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek.contains(dow + "")) {
                            hasNextDay = true;
                            break;
                        }
                    }

                    if (hasNextDay && minNextTime > nextDate.getTime() - new Date().getTime()) {
                        nextDateToRun = sdf.format(nextDate);
                        minNextTime = nextDate.getTime() - new Date().getTime();
                    }

                } catch (ParseException ex) {
                }

            }

            if (minNextTime != Long.MAX_VALUE) {
                return (int) (minNextTime / 1000);
            }

        }

        return -1;
    }

    public String getName() {
        return this.getClass().getAnnotation(FAThread.class).name() + this.hashCode();
    }

}
