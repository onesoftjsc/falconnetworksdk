/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.service;

import com.os.falcon.net.sdk.config.ConfigManager;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.os.falcon.net.sdk.config.ClientConfigItem;
import com.os.falcon.net.sdk.config.ServerConfigItem;
import com.os.falcon.net.sdk.message.Message;
import com.os.falcon.net.sdk.telnet.TelnetManager;
import com.os.falcon.net.sdk.threadmanagement.FThreadManager;
import com.os.falcon.net.sdk.util.LogUtil;
import com.os.falcon.net.sdk.zeromq.ZeroMQClient;
import com.os.falcon.net.sdk.zeromq.ZeroMQServer;
import io.socket.client.Socket;

/**
 *
 * @author quanph (quanph@onesoft.com.vn)
 */
public class FalconNetworkSDK {

    public static void init() throws Exception {
        ConfigManager.getInstance().loadConfig();
        NetworkManager.getInstance().start();
        TelnetManager.getInstance().start();
        FThreadManager.getInstance().start();
    }

    public static SocketIOServer listenOn(int port, String context) throws Exception {
        return NetworkManager.getInstance().startServer(ServerConfigItem.createItem("0.0.0.0", port, context, false, "", ""));
    }

    public static Socket connectTo(String uri) throws Exception {
        return NetworkManager.getInstance().startClient(ClientConfigItem.createItem(uri));
    }

    public static void startClientOnUri(String uri) {

    }

    public static void sendToClient(SocketIOClient client, Message message) {

        if (client == null || !client.isChannelOpen()) {
            return;
        }

        client.sendEvent(message.getEvent(), message);
    }

    public static void sendToServer(Socket socket, Message message) {
        if (socket == null || !socket.connected() || !NetworkManager.getInstance().mapSocketToMessageQueue.containsKey(socket)) {
            LogUtil.info("Miss!!");
            return;
        }
        NetworkManager.getInstance().mapSocketToMessageQueue.get(socket).add(message);
    }

//    public static ZeroMQServer listenOnZeroMQ(int port) {
//
//    }
//
//    public static ZeroMQClient connectToZeroMQ(int port) {
//
//    }
//
//    public static void sendToClientZeroMQ(ZeroMQClient client, Message message) {
//
//    }
//
//    public static void sendToServerZeroMQ(ZeroMQServer server, Message message) {
//
//    }

}
