/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.service;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ExceptionListener;
import com.os.falcon.net.sdk.util.LogUtil;
import io.netty.channel.ChannelHandlerContext;
import java.util.List;

/**
 *
 * @author quanph (quanph@onesoft.com.vn)
 */
public class FalconNetworkExceptionListener implements ExceptionListener{

    @Override
    public void onEventException(Exception excptn, List<Object> list, SocketIOClient sioc) {
        LogUtil.error(excptn);
    }

    @Override
    public void onDisconnectException(Exception excptn, SocketIOClient sioc) {
                LogUtil.error(excptn);

    }

    @Override
    public void onConnectException(Exception excptn, SocketIOClient sioc) {
        LogUtil.error(excptn);

    }

    @Override
    public void onPingException(Exception excptn, SocketIOClient sioc) {
        LogUtil.error(excptn);

    }

    @Override
    public boolean exceptionCaught(ChannelHandlerContext chc, Throwable thrwbl) throws Exception {
        return true;

    }
    
}
