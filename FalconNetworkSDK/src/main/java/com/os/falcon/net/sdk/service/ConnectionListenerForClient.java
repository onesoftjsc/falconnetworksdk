/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.service;

import io.socket.client.Socket;

/**
 *
 * @author quanph
 */
public abstract class ConnectionListenerForClient {
    public abstract void onConnected(Socket socket);
    public abstract void onError(Socket socket);
}
