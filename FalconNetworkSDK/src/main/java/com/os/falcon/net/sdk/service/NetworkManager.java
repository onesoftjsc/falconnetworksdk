package com.os.falcon.net.sdk.service;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.MapMaker;
import com.os.falcon.net.sdk.annotation.CSEvent;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import org.reflections.Reflections;

import com.os.falcon.net.sdk.annotation.SCEvent;
import com.os.falcon.net.sdk.config.ClientConfigItem;
import com.os.falcon.net.sdk.config.ConfigManager;
import com.os.falcon.net.sdk.config.FNConstant;
import com.os.falcon.net.sdk.config.ServerConfigItem;
import com.os.falcon.net.sdk.exception.MessageTypeException;
import java.util.Map;
import com.os.falcon.net.sdk.message.EventListenerItem;
import com.os.falcon.net.sdk.message.Message;
import com.os.falcon.net.sdk.socket.listener.CSMessageListener;
import com.os.falcon.net.sdk.socket.listener.SCMessageListener;
import com.os.falcon.net.sdk.socket.listener.FConnectListener;
import com.os.falcon.net.sdk.util.LogUtil;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.WebSocket;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author quanph (quanph@onesoft.com.vn)
 */
public class NetworkManager {

    private static NetworkManager _instance = null;

    List<SocketIOServer> servers = new ArrayList<>();
    List<Socket> clients = new ArrayList<>();

    public Map<String, List<EventListenerItem>> mapEvtNameToEvent = new MapMaker().makeMap();
    public Map<Socket, Queue<Message>> mapSocketToMessageQueue = new MapMaker().concurrencyLevel(4).makeMap();

    public NetworkManager() {
    }

    public static NetworkManager getInstance() {
        if (_instance == null) {
            _instance = new NetworkManager();
        }
        return _instance;
    }

    public void start() throws Exception {
        for (ServerConfigItem serverItem : ConfigManager.getInstance().getServerConfigs()) {
            startServer(serverItem);
        }

        for (ClientConfigItem clientItem : ConfigManager.getInstance().getClientConfigs()) {
            startClient(clientItem);
        }
    }

    SocketIOServer startServer(ServerConfigItem serverItem) throws Exception {

        Configuration configuration = new Configuration();
        configuration.setHostname(serverItem.getServerIp());
        configuration.setPort(serverItem.getServerPort());
        configuration.setContext(serverItem.getServerContextPath());
        configuration.setTransports(Transport.WEBSOCKET);

//        configuration.setExceptionListener(new FNetExceptionListener());
        if (serverItem.isSslOn()) {
            String fileKeyStore = System.getProperty("user.dir") + File.separator + serverItem.getSslKeystore();
            configuration.setKeyStore(new FileInputStream(fileKeyStore));
            configuration.setKeyStorePassword(serverItem.getSslPassword());
        }

        SocketIOServer server = new SocketIOServer(configuration);
        server.addConnectListener(new FConnectListener());

        Reflections reflections = new Reflections(FNConstant.PACKAGE_TO_SCAN);
        Set<Class<?>> eventClass = reflections.getTypesAnnotatedWith(CSEvent.class);
        List<Class<?>> listClass = new ArrayList<>();
        for (Class item : eventClass) {
            listClass.add(item);
        }

        Collections.sort(listClass, new Comparator<Class<?>>() {
            public int compare(Class<?> cl1, Class<?> cl2) {
                int p1 = cl1.getAnnotation(CSEvent.class).priority();
                int p2 = cl2.getAnnotation(CSEvent.class).priority();
                return p1 - p2;
            }
        });

        for (Class item : listClass) {
            CSEvent event = (CSEvent) item.getAnnotation(CSEvent.class);
            CSMessageListener dataListener = (CSMessageListener) item.newInstance();
            dataListener.setServer(server);

            Type genericSuperClass = dataListener.getClass().getGenericSuperclass();
            if (genericSuperClass instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericSuperClass).getActualTypeArguments();
                Class csMessageClass = (Class) genericTypes[0];

                for (Field f : csMessageClass.getDeclaredFields()) {
                    if (f.getType() == float.class) {
                        throw new MessageTypeException(MessageTypeException.FLOAT_UNSUPPORTED + ". Please check the class " + csMessageClass.getSimpleName());
                    }
                    if (f.getType() == Date.class) {
                        throw new MessageTypeException(MessageTypeException.DATE_UNSUPPORTED + ". Please check the class " + csMessageClass.getSimpleName());
                    }
                }

                server.addEventListener(event.name(), csMessageClass, dataListener);

//                    if (!mapEvtNameToEvent.containsKey(event.name())) {
//                        mapEvtNameToEvent.put(event.name(), new ArrayList<>());
//                    }
//                    mapEvtNameToEvent.get(event.name()).add(new EventListenerItem(csMessageClass, dataListener));
            }

        }

        Set<Class<? extends ConnectionListenerForServer>> connectionEventClass = reflections.getSubTypesOf(ConnectionListenerForServer.class);
        for (Class item : connectionEventClass) {
            ConnectionListenerForServer scl = (ConnectionListenerForServer) item.newInstance();
            scl.setServer(server);
            server.addPingListener(scl);
            server.addConnectListener(scl);
            server.addDisconnectListener(scl);
        }

        server.start();
        LogUtil.info("Server started at port " + serverItem.getServerPort());
        return server;
    }

    Socket startClient(ClientConfigItem clientItem) throws Exception {
        String contextPath = clientItem.getUri().substring(clientItem.getUri().lastIndexOf("/"));
        String xUri = clientItem.getUri().substring(0, clientItem.getUri().lastIndexOf("/"));
        IO.Options opts = new IO.Options();
//        opts.forceNew = true;
        opts.path = contextPath;
        opts.reconnection = true;
        opts.transports = new String[]{WebSocket.NAME, Polling.NAME};

        Socket socket = IO.socket(xUri, opts);

        Reflections reflections = new Reflections(FNConstant.PACKAGE_TO_SCAN);

        Set<Class<?>> eventClass = reflections.getTypesAnnotatedWith(SCEvent.class);
        List<Class<?>> listClass = new ArrayList<>();
        for (Class item : eventClass) {
            listClass.add(item);
        }

        Collections.sort(listClass, new Comparator<Class<?>>() {
            public int compare(Class<?> cl1, Class<?> cl2) {
                int p1 = cl1.getAnnotation(SCEvent.class).priority();
                int p2 = cl2.getAnnotation(SCEvent.class).priority();
                return p1 - p2;
            }
        });

        for (Class item : listClass) {
            SCEvent event = (SCEvent) item.getAnnotation(SCEvent.class);
            SCMessageListener dataListener = (SCMessageListener) item.newInstance();
            dataListener.setSocket(socket);

            Type genericSuperClass = dataListener.getClass().getGenericSuperclass();
            if (genericSuperClass instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericSuperClass).getActualTypeArguments();
                Class scMessageClass = (Class) genericTypes[0];

                for (Field f : scMessageClass.getDeclaredFields()) {
                    if (f.getType() == float.class) {
                        throw new MessageTypeException(MessageTypeException.FLOAT_UNSUPPORTED + ". Please check the class " + scMessageClass.getSimpleName());
                    }
                    if (f.getType() == Date.class) {
                        throw new MessageTypeException(MessageTypeException.DATE_UNSUPPORTED + ". Please check the class " + scMessageClass.getSimpleName());
                    }
                }

                dataListener.setMessageClass(scMessageClass);
                socket.on(event.name(), dataListener);

//                    if (!mapEvtNameToEvent.containsKey(event.name())) {
//                        mapEvtNameToEvent.put(event.name(), new ArrayList<>());
//                    }
//                    mapEvtNameToEvent.get(event.name()).add(new EventListenerItem(csMessageClass, dataListener));
            }

        }

        Set<Class<? extends ConnectionListenerForClient>> connectionEventClass = reflections.getSubTypesOf(ConnectionListenerForClient.class);
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                LogUtil.info("Connected to " + clientItem.getUri());
                if (!mapSocketToMessageQueue.containsKey(socket)) {
                    mapSocketToMessageQueue.put(socket, EvictingQueue.create(10000000));
                    new Thread(() -> {
                        while (true) {
                            if (!socket.connected() || !mapSocketToMessageQueue.containsKey(socket) || mapSocketToMessageQueue.get(socket).size() == 0) {
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException ex) {
                                }
                            } else {
                                Message message = mapSocketToMessageQueue.get(socket).poll();
                                socket.emit(message.getEvent(), new JSONObject(message));
                            }
                        }
                    }).start();
                }
                for (Class cls : connectionEventClass) {
                    try {
                        ConnectionListenerForClient ins = (ConnectionListenerForClient) cls.newInstance();
                        ins.onConnected(socket);
                    } catch (Exception ex) {

                    }

                }
            }
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                for (int i = 0; i < os.length; i++) {
                    LogUtil.info("ERROR: " + os[i]);
                }
                LogUtil.info("Error!");
                for (Class cls : connectionEventClass) {
                    try {
                        ConnectionListenerForClient ins = (ConnectionListenerForClient) cls.newInstance();
                        ins.onError(socket);

                    } catch (Exception ex) {

                    }

                }
            }
        }).on(Socket.EVENT_PING, (os) -> {
            socket.emit(Socket.EVENT_PONG);
        }).on(Socket.EVENT_RECONNECT, (os) -> {
            for (int i = 0; i < os.length; i++) {
                LogUtil.info("RECONNECT: " + os[i]);
            }
        }).on(Socket.EVENT_CONNECTING, (os) -> {
            for (int i = 0; i < os.length; i++) {
                LogUtil.info("RECONNECTING: " + os[i]);
            }
        });

        socket.connect();
        return socket;
    }

}
