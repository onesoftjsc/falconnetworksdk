/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.config;

import com.os.falcon.net.sdk.util.LogUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author quanph (quanph@onesoft.com.vn)
 */
public class ConfigManager {

    private static ConfigManager _instance = null;

    private List<ClientConfigItem> clientConfigs = new ArrayList<>();
    private List<ServerConfigItem> serverConfigs = new ArrayList<>();

    public static ConfigManager getInstance() {
        if (_instance == null) {
            _instance = new ConfigManager();
            _instance.loadConfig();
        }
        return _instance;
    }

    private int telnetPort = 22222;

    public void loadConfig() {

        try {
            File fXmlFile = new File("conf/server.xml");
            if (fXmlFile.exists()) {
                LogUtil.info("Found conf/server.xml");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("server");
                for (int i = 0; i < nList.getLength(); i++) {
                    ServerConfigItem serverItem = new ServerConfigItem();
                    Node nNode = nList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;
                        serverItem.serverIp = eElement.getElementsByTagName("serverIp").item(0).getTextContent().trim();
                        serverItem.serverPort = Integer.parseInt(eElement.getElementsByTagName("serverPort").item(0).getTextContent().trim());
                        serverItem.serverContextPath = eElement.getElementsByTagName("serverContextPath").item(0).getTextContent().trim();
                        serverItem.sslOn = Integer.parseInt(eElement.getElementsByTagName("on").item(0).getTextContent().trim()) == 1;
                        serverItem.sslKeystore = eElement.getElementsByTagName("sslKeystore").item(0).getTextContent().trim();
                        serverItem.sslPassword = eElement.getElementsByTagName("sslPassword").item(0).getTextContent().trim();
                        serverConfigs.add(serverItem);
                    }
                }

                NodeList nListTelnet = doc.getElementsByTagName("telnet");

                Node nNodeTelnet = nListTelnet.item(0);
                if (nNodeTelnet.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNodeTelnet;
                    telnetPort = Integer.parseInt(eElement.getElementsByTagName("port").item(0).getTextContent().trim());

                }
            }

            File fXmlFileClient = new File("conf/client.xml");
            if (fXmlFileClient.exists()) {
                LogUtil.info("Found conf/client.xml");
                DocumentBuilderFactory dbFactoryClient = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilderClient = dbFactoryClient.newDocumentBuilder();
                Document docClient = dBuilderClient.parse(fXmlFileClient);
                docClient.getDocumentElement().normalize();

                NodeList nListClient = docClient.getElementsByTagName("client");
                for (int i = 0; i < nListClient.getLength(); i++) {
                    ClientConfigItem clientItem = new ClientConfigItem();
                    Node nNode = nListClient.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;
                        clientItem.uri = eElement.getElementsByTagName("uri").item(0).getTextContent().trim();

                        clientConfigs.add(clientItem);
                    }
                }
            }

        } catch (Exception ex) {
            LogUtil.error(ex.getMessage(), ex);
        }
    }

    public List<ClientConfigItem> getClientConfigs() {
        return clientConfigs;
    }

    public List<ServerConfigItem> getServerConfigs() {
        return serverConfigs;
    }

    public int getTelnetPort(){
        return telnetPort;
    }
    
    public static void main(String[] args){
        System.out.println(ConfigManager.getInstance().getTelnetPort());
    }
}
