/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.config;

/**
 *
 * @author quanph
 */
public class ClientConfigItem {
     String uri;

    public String getUri() {
        return uri;
    }
    
    public static ClientConfigItem createItem(String uri){
        ClientConfigItem item = new ClientConfigItem();
        item.uri = uri;
        return item;
    }
    
}
