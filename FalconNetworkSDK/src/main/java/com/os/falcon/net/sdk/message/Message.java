/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public abstract class Message {
    private long remoteTime = System.currentTimeMillis();
    private String remoteTimeString = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());

    public long getRemoteTime() {
        return remoteTime;
    }

    public void setRemoteTime(long remoteTime) {
        this.remoteTime = remoteTime;
    }

    public String getRemoteTimeString() {
        return remoteTimeString;
    }

    public void setRemoteTimeString(String remoteTimeString) {
        this.remoteTimeString = remoteTimeString;
    }

    

    @JsonIgnore
    public abstract String getEvent();


}
