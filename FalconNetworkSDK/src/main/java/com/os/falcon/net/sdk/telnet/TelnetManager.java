package com.os.falcon.net.sdk.telnet;

import com.os.falcon.net.sdk.config.ConfigManager;
import com.os.falcon.net.sdk.util.LogUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.net.InetSocketAddress;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public class TelnetManager {

    private static TelnetManager _instance = null;

    public static TelnetManager getInstance() {
        if (_instance == null) {
            _instance = new TelnetManager();
        }
        return _instance;
    }

    public void start() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new StringDecoder());
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast("handler", new TelnetServerHandler());

                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            b.bind(new InetSocketAddress(ConfigManager.getInstance().getTelnetPort()));

        } catch (Exception ex) {
            LogUtil.error(ex.getMessage(), ex);

            System.exit(0);
        }
    }

}
