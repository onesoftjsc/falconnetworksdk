package com.os.falcon.net.sdk.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public final class Password {

    //http://stackoverflow.com/a/2861125/3474
    private static final int COST = 8819;
    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final int SIZE = 128;
    private static final String SALT = "LJPBqhQ7ZVEgSBxUBgWWYUuUH9yfBPtpWZD29TPK";
    private static final Charset CHARSETS = StandardCharsets.UTF_8;

    public static String hash(String password) {
        return hash(password.toCharArray());
    }

    public static String hash(char[] password) {
        byte[] saltBytes = SALT.getBytes(CHARSETS);
        byte[] passwordBytes = String.valueOf(password).getBytes(CHARSETS);
        byte[] passwordXorWithSalt = xorWithKey(passwordBytes, saltBytes);
        password = new String(passwordXorWithSalt, CHARSETS).toCharArray();
        KeySpec spec = new PBEKeySpec(password, saltBytes, COST, SIZE);
        try {
            SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
            return new String(f.generateSecret(spec).getEncoded(), CHARSETS);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static boolean check(String password, String stored) {
        return hash(password).equals(stored);
    }

    public static boolean check(char[] password, String stored) {
        return hash(password).equals(stored);
    }

    private static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i % key.length]);
        }
        return out;
    }
}
