/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.socket.listener;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph
 * @param <T>
 */
public abstract class CSMessageListener<T extends Object> implements DataListener<T>{

    SocketIOServer server;
    
    @Override
    public void onData(SocketIOClient client, T message, AckRequest ar) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LogUtil.debug(client.hashCode() + ": Server Received: " + message.getClass().getSimpleName() + ": " + mapper.writeValueAsString(message));
        onData(client, message, server);
    }
    
    public abstract void onData(SocketIOClient client, T message, SocketIOServer server );
    public void setServer(SocketIOServer server){
        this.server = server;
    }
}
