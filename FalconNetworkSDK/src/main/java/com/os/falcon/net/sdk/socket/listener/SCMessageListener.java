/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.socket.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.net.sdk.message.Message;
import com.os.falcon.net.sdk.util.LogUtil;
import io.socket.emitter.Emitter;
import io.socket.client.Socket;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quanph
 * @param <T>
 */
public abstract class SCMessageListener<T extends Object> implements Emitter.Listener {

    Socket socket;
    private Class messageClass;
    
    @Override
    public void call(Object... os) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Object message  = objectMapper.readValue(os[0].toString(), messageClass);
            LogUtil.debug(socket.hashCode() + ": Client Received: " + message.getClass().getSimpleName()+ ": " + os[0].toString());
            onData(socket, (T)message);
        } catch (IOException ex) {
            Logger.getLogger(SCMessageListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract void onData(Socket socket, T message);

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
    
    public void setMessageClass(Class messageClass){
        this.messageClass = messageClass;
    }
}
