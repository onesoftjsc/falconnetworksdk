/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.threadmanagement;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public interface IFThread {
    public void process();
    public void onStart();
    public void onStop();
}
