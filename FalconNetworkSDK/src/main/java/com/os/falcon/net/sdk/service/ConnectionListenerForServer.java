/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.service;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.corundumstudio.socketio.listener.PingListener;

/**
 *
 * @author quanph
 */
public abstract class ConnectionListenerForServer implements PingListener, ConnectListener, DisconnectListener{

    SocketIOServer server;
    
    public abstract void  onPing(SocketIOClient client, SocketIOServer server);
    public abstract void  onConnect(SocketIOClient client, SocketIOServer server);
    public abstract void  onDisconnect(SocketIOClient client, SocketIOServer server);
    
    @Override
    public void onPing(SocketIOClient client) {
        onPing(client, server);
    }

    @Override
    public void onConnect(SocketIOClient client) {
        onConnect(client, server);
    }

    @Override
    public void onDisconnect(SocketIOClient client) {
        onDisconnect(client, server);
    }

    public void setServer(SocketIOServer server) {
        this.server = server;
    }
    
    
    
    
    
}
