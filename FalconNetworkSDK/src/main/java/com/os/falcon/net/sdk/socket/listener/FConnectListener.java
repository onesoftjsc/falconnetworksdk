/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.socket.listener;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph (quanph@onesoft.com.vn)
 */
public class FConnectListener implements ConnectListener{

    @Override
    public void onConnect(SocketIOClient sioc) {
        LogUtil.info("New Connection @@@");
    }
    
}
