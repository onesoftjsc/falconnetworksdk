Lưu ý: Copy 3 file client.xml, server.xml, falcon_log4j.properties trong thư mục conf
trong file jar vào thư mục conf/ trên thư mục của ứng dụng.


1. Thư viện mạng
    1.1 Hỗ trợ cả Client và Server
    1.2 Hỗ trợ multi Server và multi Client cùng lúc
    1.3 Khởi tạo kết nối
        1.3.1 Nếu sử dụng file cấu hình conf/server.xml và conf/client.xml thì
                gọi hàm FalconNetworkSDK.init() để khởi tạo Server và Client
                Ví dụ file server.xml và client.xml kèm theo trong thư viện.
        1.3.2 Có thể sử dụng hàm FalconNetworkSDK.listenOn(int port, String context)
                để khởi tạo Server
              Có thể sử dụng hàm FalconNetworkSDK.connectTo(String url) để khởi
                tạo Client kết nối tới Server tại địa chỉ url
    1.4 Sự kiện và gửi nhận bản tin
        1.4.1 Các class có prefix là CS liên quan tới bản tin từ Client -> Server
              Các class có prefix là SC liên quan tới bản tin từ Server -> Client
        1.4.2 Lắng nghe các sự kiện kết nối (Connected, Disconnect, Error)
              Nếu ứng dụng của bạn có vai trò là Client, thì sự kiện kết nối cần
              kế thừa class ConnectionListenerForClient
              Nếu ứng dụng của bạn có vai trò là Client, thì sự kiện kết nối cần
              kế thừa class ConnectionListenerForServer
        1.4.3 Các bản tin truyền đi giữa Client và Server cần kế thừa class Message.
              Các bản tin cần overide lại method getEvent() trả về kiểu String 
              định danh cho bản tin đó.
        1.4.4 Nếu ứng dụng của bạn có vai trò là Server, các sự kiện lắng nghe
              bản tin cần kế thừa class CSMessageEvent và khai báo Annotation @CSEvent
              Ví dụ: @CSEvent(name = "cs_ad_log")
                     public class AdsLogEvent extends CSMessageListener<CSAdsDwhLog>
              Nếu ứng dụng của bạn có vai trò là Client, các sự kiện lắng nghe
              bản tin cần kế thừa class SCMessageEvent và khai báo Annotation @SCEvent
              Ví dụ: @SCEvent(name = "sc_ad_log")
                     public class AdsLogEvent extends SCMessageListener<SCAdsDwhLog>
        1.4.5 Để gửi nhận bản tin giữa Client và Server, cần sử dụng 
              FalconNetworkSDK.sendToClient và FalconNetworkSDK.sendToServer
 
             
