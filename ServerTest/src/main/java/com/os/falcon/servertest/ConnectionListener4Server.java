/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.os.falcon.net.sdk.service.ConnectionListenerForServer;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph
 */
public class ConnectionListener4Server extends ConnectionListenerForServer{

    @Override
    public void onPing(SocketIOClient client, SocketIOServer server) {
        client.sendEvent("pong");
    }

    @Override
    public void onConnect(SocketIOClient client, SocketIOServer server) {
        LogUtil.info("onConnect: " + client + "," + server);
    }

    @Override
    public void onDisconnect(SocketIOClient client, SocketIOServer server) {
        LogUtil.info("onDisconnect : " + client + "," + server);
    }
    
}
