/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

/**
 *
 * @author quanph
 */
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class Server {

    ZContext context = new ZContext(1);
    ZMQ.Socket socket = context.createSocket(ZMQ.PAIR);

    public static void main(String args[]) {
        Server server = new Server();
        server.start();

    }

    public void start() {

        // Socket to talk to clients
        socket.bind("tcp://*:10101");
        int count = 0;
        while (true) {
            
            byte[] recv = socket.recv();
            count++;
            if (count % 1000 == 0){
                System.out.println(count);
            }
        }
    }

    public void stop() {
        socket.close();
        context.close();
    }
}
