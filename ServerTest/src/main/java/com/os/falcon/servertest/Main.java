/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

import com.os.falcon.net.sdk.service.FalconNetworkSDK;
import com.os.falcon.net.sdk.threadmanagement.FThreadManager;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph <quanph@onesoft.com.vn>
 */
public class Main {

    public static void main(String[] args) {
        try {
            FalconNetworkSDK.listenOn(10443, "/test");
            FThreadManager.getInstance().start();


        } catch (Exception ex) {
            LogUtil.error(ex.getMessage(), ex);
        }
    }
}
