/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.os.falcon.net.sdk.message.Message;

/**
 *
 * @author quanph <quanph@onesoft.com.vn>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CSPing extends Message {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CSPing() {
    }

    @Override
    public String getEvent() {
        return "test";
    }

}
