/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

import com.os.falcon.net.sdk.annotation.FAAutorun;
import com.os.falcon.net.sdk.annotation.FADailySchedule;
import com.os.falcon.net.sdk.annotation.FAFixedRateSchedule;
import com.os.falcon.net.sdk.annotation.FAThread;
import com.os.falcon.net.sdk.threadmanagement.FThread;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph
 */
@FAThread(name = "MonitorThread")
@FAAutorun
@FAFixedRateSchedule(period = 1)
public class MonitorThread extends FThread{
    public static int count = 0;
    @Override
    public void process() {
        LogUtil.info("Count: " + count);
    }

    @Override
    public void onStart() {
    }
    
}
