/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.servertest;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.os.falcon.net.sdk.annotation.CSEvent;
import com.os.falcon.net.sdk.service.FalconNetworkSDK;
import com.os.falcon.net.sdk.socket.listener.CSMessageListener;
import com.os.falcon.net.sdk.util.LogUtil;

/**
 *
 * @author quanph <quanph@onesoft.com.vn>
 */
@CSEvent(name = "test")
public class PingEvent extends CSMessageListener<CSPing> {

    @Override
    public void onData(SocketIOClient client, CSPing message, SocketIOServer server) {
//        LogUtil.info("Message: " + message.getText());
MonitorThread.count++;
    }

}
